import java.util.*;
import java.util.stream.Collectors;

public class FilterCollection {
    
    public static List<Integer> getAllEvens(List<Integer> list) {
        return list.stream().filter(x -> x % 2 == 0).collect(Collectors.toList());
    }
    
    public static List<Integer> removeDuplicateElements(List<Integer> list) {
//        Set<Integer> set = new HashSet<>(list);
//        return new ArrayList<>(set);
        return list.stream().distinct().collect(Collectors.toList());
    }
    
    public static List<Integer> getCommonElements(List<Integer> collection1, List<Integer> collection2) {
//        List<Integer> collection = new ArrayList<>(collection1);
//        collection.retainAll(collection2);
//        return collection;
        return collection1.stream().filter(collection2::contains).collect(Collectors.toList());
    }
}
