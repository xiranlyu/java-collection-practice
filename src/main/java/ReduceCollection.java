import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.ToDoubleFunction;
import java.util.stream.Stream;

public class ReduceCollection {
    public static int getMax(List<Integer> list) {
//        return Collections.max(list);
        return list.stream().reduce(list.get(0), (a, b) -> a > b ? a : b);
    }
    
    public static double getAverage(List<Integer> list) {
        return list.stream().mapToDouble(Integer::doubleValue).average().orElse(Double.NaN);
    }
    
    public static int getSum(List<Integer> list) {
        return list.stream().reduce(0, Integer::sum);
    }
    
    public static double getMedian(List<Integer> list) {
        int median = 0;
        if (list.size() % 2 == 1) {
            median = list.get(list.size() / 2);
        } else {
            median = (list.get(list.size() / 2 - 1) + list.get(list.size() / 2)) / 2 ;
        }
        return median;
    }
    
    public static int getFirstEven(List<Integer> list) {
        return list.stream().filter(x -> x % 2 == 0).findFirst().orElse(0);
    }
}
