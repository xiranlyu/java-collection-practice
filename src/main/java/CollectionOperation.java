import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toCollection;

public class CollectionOperation {

    public static List<Integer> getListByInterval(int left, int right) {
        List<Integer> intList = new ArrayList<>();
        if (left < right) {
            for (int i = left; i <= right; i++) {
                intList.add(i);
            }
        } else {
            for (int i = left; i >= right; i--) {
                intList.add(i);
            }
        }
        return intList;
    }


    public static List<Integer> removeLastElement(List<Integer> list) {
        return list.stream().limit(list.size() - 1).collect(Collectors.toList());
    }

    public static List<Integer> sortDesc(List<Integer> list) {
        return list.stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
    }

    public static List<Integer> reverseList(List<Integer> list) {
        Collections.reverse(list);
        return list;
    }

    public static List<Integer> concat(List<Integer> list1, List<Integer> list2) {
        return Stream.concat(list1.stream(), list2.stream()).collect(Collectors.toList());
    }

    public static List<Integer> union(List<Integer> list1, List<Integer> list2) {
        return Stream.concat(list1.stream(), list2.stream()).distinct().collect(Collectors.toList());
    }

    public static boolean isAllElementsEqual(List<Integer> list1, List<Integer> list2) {
        return list2.containsAll(list1);
    }
}
